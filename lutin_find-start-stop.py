#!/usr/bin/python
import lutin.module as module
import lutin.tools as tools
import lutin.debug as debug
import os

def get_type():
	return "BINARY"

def get_name():
	return "find start stop"

def get_desc():
	return "finder of start and stop in tv stream with ffmpeg to auto cut stream"

def get_licence():
	return "GPL v3"

def get_compagny_type():
	return "org"

def get_compagny_name():
	return "Edouard DUPIN"

def get_maintainer():
	return ["Mr DUPIN Edouard <yui.heero@gmail.com>"]

def get_version():
	return [0,1,"dev"]

def configure(target, my_module):
	my_module.add_src_file([
	    'appl/debug.cpp',
	    'appl/Main.cpp',
	    ])
	my_module.add_depend([
	    'etk',
	    'ffmpeg-libs',
	    'opencv-imgproc'
	    ])
	my_module.add_path(".")
	my_module.add_depend([
	    'ffmpeg-libs',
	    'va',
	    'vdpau',
	    'egami'
	    ])
	my_module.add_flag('link', [
	    "-ldl"
	    ])
	return True

