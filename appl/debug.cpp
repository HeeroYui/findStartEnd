/** @file
 * @author Edouard DUPIN
 * @copyright 2011, Edouard DUPIN, all right reserved
 * @license NOT FREE : Private Sources  == > you can not redistribute sources or binaries
 */

#include <appl/debug.hpp>

int32_t appl::getLogId() {
	static int32_t g_val = elog::registerInstance("find-start-stop");
	return g_val;
}

