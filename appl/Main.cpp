/** @file
 * @author Edouard DUPIN
 * @copyright 2011, Edouard DUPIN, all right reserved
 * @license NOT FREE : Private Sources  == > you can not redistribute sources or binaries
 */
#include <etk/types.hpp>
#include <etk/etk.hpp>
#include <etk/os/FSNode.hpp>
#include <appl/debug.hpp>
#include <appl/Main.hpp>
extern "C" {
	#include <libavutil/imgutils.h>
	#include <libavutil/samplefmt.h>
	#include <libavutil/timestamp.h>
	#include <libavformat/avformat.h>
	#include <libavutil/pixdesc.h>
	#include <libswscale/swscale.h>
}
// Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/core/core.hpp>
// Gaussian Blur
#include <opencv2/imgproc/imgproc.hpp>
#include <egami/egami.hpp>

int32_t factor = 16;


static AVFormatContext *fmt_ctx = nullptr;
static AVCodecContext *video_dec_ctx = nullptr, *audio_dec_ctx;
static int width, height;
static enum AVPixelFormat pix_fmt;
static AVStream *video_stream = nullptr, *audio_stream = nullptr;
static std::string src_filename;
static const char *video_dst_filename = nullptr;
static const char *audio_dst_filename = nullptr;
static FILE *video_dst_file = nullptr;
static FILE *audio_dst_file = nullptr;

static uint8_t *videoDstData[4] = {nullptr};
static int videoDstLineSize[4];
static int videoDstBufsize;
static uint8_t *videoDstDataPrevious[4] = {nullptr};
static int videoDstLineSizePrevious[4];
static int videoDstBufsizePrevious;

static int video_stream_idx = -1, audio_stream_idx = -1;
static AVFrame *frame = nullptr;
static AVPacket pkt;
static int videoFrameCount = 0;
static int audio_frame_count = 0;

/* Enable or disable frame reference counting. You are not supposed to support
 * both paths in your application but pick the one most appropriate to your
 * needs. Look for the use of refcount in this example to see what are the
 * differences of API usage between them. */
static int refcount = 0;



static const char* get_audio_format(enum AVSampleFormat _sampleFmt) {
	int i;
	struct sample_fmt_entry {
		enum AVSampleFormat sample_fmt;
		const char* fmt_be;
		const char* fmt_le;
	} sample_fmt_entries[] = {
		{ AV_SAMPLE_FMT_U8,  "u8",    "u8"},
		{ AV_SAMPLE_FMT_S16, "s16be", "s16le" },
		{ AV_SAMPLE_FMT_S32, "s32be", "s32le" },
		{ AV_SAMPLE_FMT_FLT, "f32be", "f32le" },
		{ AV_SAMPLE_FMT_DBL, "f64be", "f64le" },
	};
	for (i = 0; i < FF_ARRAY_ELEMS(sample_fmt_entries); i++) {
		struct sample_fmt_entry *entry = &sample_fmt_entries[i];
		if (_sampleFmt == entry->sample_fmt) {
			return AV_NE(entry->fmt_be, entry->fmt_le);
		}
	}
	APPL_DEBUG("sample format " << av_get_sample_fmt_name(_sampleFmt) << " is not supported as output format");
	return nullptr;
}

// http://docs.opencv.org/2.4/doc/tutorials/highgui/video-input-psnr-ssim/video-input-psnr-ssim.html#videoinputpsnrmssim
double getPSNR(const cv::Mat& _I1, const cv::Mat& _I2) {
	cv::Mat s1;
	// |_I1 - _I2|
	cv::absdiff(_I1, _I2, s1);
	// cannot make a square on 8 bits
	s1.convertTo(s1, CV_32F);
	// |_I1 - _I2|^2
	s1 = s1.mul(s1);
	// sum elements per channel
	cv::Scalar s = cv::sum(s1);
	// sum channels
	double sse = s.val[0] + s.val[1] + s.val[2];
	// for small values return zero
	if (sse <= 1e-10) {
		return 0;
	} else {
		double mse  = sse / (double)(_I1.channels() * _I1.total());
		double psnr = 10.0 * log10((255 * 255) / mse);
		return psnr;
	}
}

cv::Scalar getMSSIM( const cv::Mat& _i1, const cv::Mat& _i2) {
	const double C1 = 6.5025;
	const double C2 = 58.5225;
	/***************************** INITS **********************************/
	int d = CV_32F;
	cv::Mat I1, I2;
	// cannot calculate on one byte large values
	_i1.convertTo(I1, d);
	_i2.convertTo(I2, d);
	// I2^2
	cv::Mat I2_2 = I2.mul(I2);
	// I1^2
	cv::Mat I1_2 = I1.mul(I1);
	// I1 * I2
	cv::Mat I1_I2 = I1.mul(I2);
	/*************************** END INITS **********************************/
	// PRELIMINARY COMPUTING
	cv::Mat mu1, mu2;
	GaussianBlur(I1, mu1, cv::Size(11, 11), 1.5);
	GaussianBlur(I2, mu2, cv::Size(11, 11), 1.5);
	cv::Mat mu1_2 = mu1.mul(mu1);
	cv::Mat mu2_2 = mu2.mul(mu2);
	cv::Mat mu1_mu2 = mu1.mul(mu2);
	cv::Mat sigma1_2, sigma2_2, sigma12;
	GaussianBlur(I1_2, sigma1_2, cv::Size(11, 11), 1.5);
	sigma1_2 -= mu1_2;
	GaussianBlur(I2_2, sigma2_2, cv::Size(11, 11), 1.5);
	sigma2_2 -= mu2_2;
	GaussianBlur(I1_I2, sigma12, cv::Size(11, 11), 1.5);
	sigma12 -= mu1_mu2;
	///////////////////////////////// FORMULA ////////////////////////////////
	cv::Mat t1, t2, t3;
	// t3 = ((2*mu1_mu2 + C1).*(2*sigma12 + C2))
	t1 = 2 * mu1_mu2 + C1;
	t2 = 2 * sigma12 + C2;
	t3 = t1.mul(t2);
	// t1 =((mu1_2 + mu2_2 + C1).*(sigma1_2 + sigma2_2 + C2))
	t1 = mu1_2 + mu2_2 + C1;
	t2 = sigma1_2 + sigma2_2 + C2;
	t1 = t1.mul(t2);
	// ssim_map = t3./t1;
	cv::Mat ssim_map;
	divide(t3, t1, ssim_map);
	// mssim = average of ssim map
	cv::Scalar mssim = mean(ssim_map);
	return mssim;
}



static int decode_packet(int* _gotFrame, int _cached) {
	int ret = 0;
	int decoded = pkt.size;

	*_gotFrame = 0;

	if (pkt.stream_index == video_stream_idx) {
		/* decode video frame */
		ret = avcodec_decode_video2(video_dec_ctx, frame, _gotFrame, &pkt);
		if (ret < 0) {
			APPL_ERROR("Error decoding video frame ");// << av_err2str(ret));
			return ret;
		}

		if (*_gotFrame) {

			if (    frame->width != width
			     || frame->height != height
			     || frame->format != pix_fmt) {
				/* To handle this change, one could call av_image_alloc again and
				 * decode the following frames into another rawvideo file. */
				APPL_ERROR("Error: Width, height and pixel format have to be constant in a rawvideo file, but the width, height or pixel format of the input video changed:");
				APPL_ERROR("    old: width = " << width << ", height = " << height << ", format = " << av_get_pix_fmt_name(pix_fmt));
				APPL_ERROR("    new: width = " << frame->width << ", height = " << frame->height << ", format = " << av_get_pix_fmt_name((enum AVPixelFormat)frame->format));
				exit(-1);
			}
			//APPL_PRINT(" get frame VIDEO");
			if (true) {
				/*
				uint8_t *imageOutData[4] = {nullptr};
				int imageOutLinesize[4];
				int rettt = av_image_alloc(imageOutData, imageOutLinesize, width, height, AV_PIX_FMT_RGB24, 1);
				if (rettt < 0) {
					APPL_ERROR("Could not allocate raw video buffer");
					exit(-1);
				}
				*/
				// convert the image format:
				SwsContext* convertCtx = sws_getContext(width, height, pix_fmt, // AV_PIX_FMT_YUV42 for pix_fmt
				                                        width/factor, height/factor, AV_PIX_FMT_RGB24,
				                                        0, 0, 0, 0);
				//sws_scale(convertCtx, (const uint8_t **)(frame->data), frame->linesize, 0, height, imageOutData, imageOutLinesize);
				sws_scale(convertCtx, (const uint8_t **)(frame->data), frame->linesize, 0, height, videoDstData, videoDstLineSize);
				
				APPL_INFO("video_frame (" << (_cached ? "(_cached)" : "") << ") n:"
				          << videoFrameCount << " coded_n:" << frame->coded_picture_number
				          << " pts:" // << av_ts2timestr(frame->pts, &video_dec_ctx->time_base)
				          << " format=" << av_get_pix_fmt_name((enum AVPixelFormat)AV_PIX_FMT_RGB24));
				
				if (videoFrameCount > 0) {
					int psnrTriggerValue = 40;
					// No copy of the data just index it ...
					cv::Mat matrixData(cv::Size(width/factor, height/factor), CV_8UC3, videoDstData[0]);
					cv::Mat matrixDataPrevious(cv::Size(width/factor, height/factor), CV_8UC3, videoDstDataPrevious[0]);
					
					#if 0
						///////////////////////////////// PSNR ////////////////////////////////////////////////////
						double psnrV = getPSNR(matrixDataPrevious, matrixData);
						if (psnrV > 40 ) {
							APPL_ERROR("        separation (PSNR) : " << std::setiosflags(std::ios::fixed) << std::setprecision(3) << psnrV << " dB");
						} else {
							APPL_INFO("        separation (PSNR) : " << std::setiosflags(std::ios::fixed) << std::setprecision(3) << psnrV << " dB");
						}
					#endif
					//////////////////////////////////// MSSIM /////////////////////////////////////////////////
					cv::Scalar mssimV = getMSSIM(matrixDataPrevious, matrixData);
					
					if (    mssimV.val[2] < 0.5
					     || mssimV.val[1] < 0.5
					     || mssimV.val[0] < 0.5) {
						APPL_WARNING("                 *** change view *** " << videoFrameCount-1 << " -> " << videoFrameCount);
						APPL_INFO("        separation (MSSIM) : R " << std::setiosflags(std::ios::fixed) << std::setprecision(2) << mssimV.val[2] * 100 << "%");
						APPL_INFO("                             G " << std::setiosflags(std::ios::fixed) << std::setprecision(2) << mssimV.val[1] * 100 << "%");
						APPL_INFO("                             B " << std::setiosflags(std::ios::fixed) << std::setprecision(2) << mssimV.val[0] * 100 << "%");
						
						egami::Image img(ivec2(width/factor, height/factor), egami::colorType::RGBA8);
						for (size_t yyy=0; yyy<height/factor; ++yyy) {
							for (size_t xxx=0; xxx<width/factor; ++xxx) {
								uint8_t* pos = &(videoDstData[0][(width/factor)*3*yyy + xxx*3]);
								uint8_t r = *pos++;
								uint8_t g = *pos++;
								uint8_t b = *pos++;
								img.set(ivec2(xxx,yyy), etk::Color<uint8_t, 4>(r, g, b, 255));
							}
						}
						egami::store(img, "out/img_" + etk::to_string(videoFrameCount-1) + ".bmp");
						
						for (size_t yyy=0; yyy<height/factor; ++yyy) {
							for (size_t xxx=0; xxx<width/factor; ++xxx) {
								uint8_t* pos = &(videoDstDataPrevious[0][(width/factor)*3*yyy + xxx*3]);
								uint8_t r = *pos++;
								uint8_t g = *pos++;
								uint8_t b = *pos++;
								img.set(ivec2(xxx,yyy), etk::Color<uint8_t, 4>(r, g, b, 255));
							}
						}
						egami::store(img, "out/img_" + etk::to_string(videoFrameCount) + ".bmp");
					} else {
						APPL_DEBUG("        separation (MSSIM) : R " << std::setiosflags(std::ios::fixed) << std::setprecision(2) << mssimV.val[2] * 100 << "%");
						APPL_DEBUG("                             G " << std::setiosflags(std::ios::fixed) << std::setprecision(2) << mssimV.val[1] * 100 << "%");
						APPL_DEBUG("                             B " << std::setiosflags(std::ios::fixed) << std::setprecision(2) << mssimV.val[0] * 100 << "%");
					}
					// store data to check ... difference
					#if 0
						egami::Image img(ivec2(width, height), egami::colorType::RGB8);
						for (size_t yyy=0; yyy<height; ++yyy) {
							for (size_t xxx=0; xxx<width; ++xxx) {
								uint8_t* pos = &(videoDstDataPrevious[0][width*3*yyy + xxx*3]);
								uint8_t r = *pos++;
								uint8_t g = *pos++;
								uint8_t b = *pos++;
								img.set(ivec2(xxx,yyy), etk::Color<uint8_t, 4>(r, g, b, 255));
							}
						}
						egami::store(img, "out/img_" + etk::to_string(videoFrameCount) + ".bmp");
						
					#endif
				}
				// copy previous data ...
				memcpy(videoDstDataPrevious[0], videoDstData[0], videoDstBufsize);
				videoDstLineSizePrevious[0] = videoDstLineSize[0];
				//av_free(imageOutData[0]);
				videoFrameCount++;
			}
			
			/* write to rawvideo file */
			/*
			                          fwrite(videoDstData[0], 1, videoDstBufsize, video_dst_file);
			*/
		}
	} else if (pkt.stream_index == audio_stream_idx) {
		/* decode audio frame */
		ret = avcodec_decode_audio4(audio_dec_ctx, frame, _gotFrame, &pkt);
		if (ret < 0) {
			APPL_ERROR("Error decoding audio frame ");// << av_err2str(ret));
			return ret;
		}
		/* Some audio decoders decode only part of the packet, and have to be
		 * called again with the remainder of the packet data.
		 * Sample: fate-suite/lossless-audio/luckynight-partial.shn
		 * Also, some decoders might over-read the packet. */
		decoded = FFMIN(ret, pkt.size);

		if (*_gotFrame) {
			size_t unpadded_linesize = frame->nb_samples * av_get_bytes_per_sample((enum AVSampleFormat)frame->format);
			APPL_DEBUG("audio_frame(" << (_cached ? "(_cached)" : "") << ") n:"
			          << audio_frame_count << " nb_samples:" << frame->nb_samples
			          << " pts:" //<< av_ts2timestr(frame->pts, &audio_dec_ctx->time_base)
			          << " format=" << get_audio_format((enum AVSampleFormat)frame->format));

			/* Write the raw audio data samples of the first plane. This works
			 * fine for packed formats (e.g. AV_SAMPLE_FMT_S16). However,
			 * most audio decoders output planar audio, which uses a separate
			 * plane of audio samples for each channel (e.g. AV_SAMPLE_FMT_S16P).
			 * In other words, this code will write only the first audio channel
			 * in these cases.
			 * You should use libswresample or libavfilter to convert the frame
			 * to packed data. */
			/*
			                         fwrite(frame->extended_data[0], 1, unpadded_linesize, audio_dst_file);
			*/
			audio_frame_count++;
		}
	}

	/* If we use frame reference counting, we own the data and need
	 * to de-reference it when we don't use it anymore */
	if (*_gotFrame && refcount)
		av_frame_unref(frame);

	return decoded;
}

static int open_codec_context(int *_streamIdx, AVFormatContext *_fmtCtx, enum AVMediaType _type)
{
	int ret, stream_index;
	AVStream *st;
	AVCodecContext *dec_ctx = nullptr;
	AVCodec *dec = nullptr;
	AVDictionary *opts = nullptr;

	ret = av_find_best_stream(_fmtCtx, _type, -1, -1, nullptr, 0);
	if (ret < 0) {
		APPL_ERROR("Could not find " << av_get_media_type_string(_type) << " stream in input file '" << src_filename << "'");
		return ret;
	} else {
		stream_index = ret;
		st = _fmtCtx->streams[stream_index];

		/* find decoder for the stream */
		dec_ctx = st->codec;
		dec = avcodec_find_decoder(dec_ctx->codec_id);
		if (!dec) {
			APPL_ERROR("Failed to find " << av_get_media_type_string(_type) << " codec");
			return AVERROR(EINVAL);
		}

		/* Init the decoders, with or without reference counting */
		av_dict_set(&opts, "refcounted_frames", refcount ? "1" : "0", 0);
		if ((ret = avcodec_open2(dec_ctx, dec, &opts)) < 0) {
			APPL_ERROR("Failed to open " << av_get_media_type_string(_type) << " codec");
			return ret;
		}
		*_streamIdx = stream_index;
	}

	return 0;
}


/**
 * @brief Main of the program (This can be set in every case, but it is not used in Andoid...).
 * @param std IO
 * @return std IO
 */
int main(int _argc, const char* _argv[]) {
	// second possibility
	etk::init(_argc, _argv);
	APPL_PRINT("Start APPLICATION");

	int ret = 0, got_frame;

	for (int32_t iii=1; iii<_argc; ++iii) {
		std::string argument = _argv[iii];
		if (etk::start_with(argument, "--refcount") == true) {
			refcount = 1;
			continue;
		}
		if (etk::start_with(argument, "-") == true) {
			refcount = 1;
			continue;
		}
		if (src_filename != "") {
			APPL_ERROR(" usage : binary [options] inputFile");
			exit(-1);
		}
		src_filename = argument;
	}

	/* register all formats and codecs */
	av_register_all();

	/* open input file, and allocate format context */
	if (avformat_open_input(&fmt_ctx, src_filename.c_str(), nullptr, nullptr) < 0) {
		APPL_ERROR("Could not open source file '" << src_filename << "'");
		exit(1);
	}

	/* retrieve stream information */
	if (avformat_find_stream_info(fmt_ctx, nullptr) < 0) {
		APPL_ERROR("Could not find stream information");
		exit(1);
	}

	if (open_codec_context(&video_stream_idx, fmt_ctx, AVMEDIA_TYPE_VIDEO) >= 0) {
		video_stream = fmt_ctx->streams[video_stream_idx];
		video_dec_ctx = video_stream->codec;
		
		/*
		video_dst_file = fopen(video_dst_filename, "wb");
		if (!video_dst_file) {
			fprintf(stderr, "Could not open destination file %s\n", video_dst_filename);
			ret = 1;
			goto end;
		}
		*/

		/* allocate image where the decoded image will be put */
		width = video_dec_ctx->width;
		height = video_dec_ctx->height;
		pix_fmt = video_dec_ctx->pix_fmt;
		// Algo format :
		//pix_fmt = AV_PIX_FMT_RGB24;
		ret = av_image_alloc(videoDstData, videoDstLineSize, width/factor, height/factor, AV_PIX_FMT_RGB24/*pix_fmt*/, 1);
		if (ret < 0) {
			APPL_ERROR("Could not allocate raw video buffer");
			goto end;
		}
		videoDstBufsize = ret;
		ret = av_image_alloc(videoDstDataPrevious, videoDstLineSizePrevious, width/factor, height/factor, AV_PIX_FMT_RGB24/*pix_fmt*/, 1);
		if (ret < 0) {
			APPL_ERROR("Could not allocate raw video buffer");
			goto end;
		}
		videoDstBufsizePrevious = ret;
	}

	if (open_codec_context(&audio_stream_idx, fmt_ctx, AVMEDIA_TYPE_AUDIO) >= 0) {
		audio_stream = fmt_ctx->streams[audio_stream_idx];
		audio_dec_ctx = audio_stream->codec;
		/*
		audio_dst_file = fopen(audio_dst_filename, "wb");
		if (!audio_dst_file) {
			fprintf(stderr, "Could not open destination file %s\n", audio_dst_filename);
			ret = 1;
			goto end;
		}
		*/
	}

	/* dump input information to stderr */
	av_dump_format(fmt_ctx, 0, src_filename.c_str(), 0);

	if (    !audio_stream
	     && !video_stream) {
		APPL_ERROR("Could not find audio or video stream in the input, aborting");
		ret = 1;
		goto end;
	}

	frame = av_frame_alloc();
	if (!frame) {
		APPL_ERROR("Could not allocate frame");
		ret = AVERROR(ENOMEM);
		goto end;
	}

	/* initialize packet, set data to nullptr, let the demuxer fill it */
	av_init_packet(&pkt);
	pkt.data = nullptr;
	pkt.size = 0;
	/*
	if (video_stream) {
		printf("Demuxing video from file '%s' into '%s'\n", src_filename.c_str(), video_dst_filename);
	}
	if (audio_stream) {
		printf("Demuxing audio from file '%s' into '%s'\n", src_filename.c_str(), audio_dst_filename);
	}
	*/

	/* read frames from the file */
	while (av_read_frame(fmt_ctx, &pkt) >= 0) {
		AVPacket orig_pkt = pkt;
		do {
			ret = decode_packet(&got_frame, 0);
			if (ret < 0)
				break;
			pkt.data += ret;
			pkt.size -= ret;
		} while (pkt.size > 0);
		av_packet_unref(&orig_pkt);
	}

	/* flush _cached frames */
	pkt.data = nullptr;
	pkt.size = 0;
	do {
		decode_packet(&got_frame, 1);
	} while (got_frame);

	APPL_PRINT("Demuxing succeeded.\n");

/*
	if (audio_stream) {
		enum AVSampleFormat sfmt = audio_dec_ctx->sample_fmt;
		int n_channels = audio_dec_ctx->channels;
		const char *fmt;

		if (av_sample_fmt_is_planar(sfmt)) {
			const char *packed = av_get_sample_fmt_name(sfmt);
			printf("Warning: the sample format the decoder produced is planar "
				   "(%s). This example will output the first channel only.\n",
				   packed ? packed : "?");
			sfmt = av_get_packed_sample_fmt(sfmt);
			n_channels = 1;
		}

		fmt = get_audio_format(sfmt);
		if (fmt == nullptr) {
			goto end;
		}

		printf("Play the output audio file with the command:\n"
			   "ffplay -f %s -ac %d -ar %d %s\n",
			   fmt, n_channels, audio_dec_ctx->sample_rate,
			   audio_dst_filename);
	}
*/
end:
	avcodec_close(video_dec_ctx);
	avcodec_close(audio_dec_ctx);
	avformat_close_input(&fmt_ctx);
	/*
	if (video_dst_file) {
		fclose(video_dst_file);
	}
	if (audio_dst_file) {
		fclose(audio_dst_file);
	}
	*/
	av_frame_free(&frame);
	av_free(videoDstData[0]);
	av_free(videoDstDataPrevious[0]);

	APPL_PRINT("Stop APPLICATION");
	return ret < 0;
}
